FROM alpine:3 as build

WORKDIR /work

COPY install.sh .

RUN apk --no-cache add \
      bash~=5 \
      curl~=7 \
  && chmod +x install.sh \
  && bash install.sh

FROM alpine:3

ENV container docker

COPY --from=build /work/tflint /usr/local/bin/tflint

WORKDIR /work
ENTRYPOINT ["tflint"]
CMD ["--version"]

ARG BUILD_DATE
ARG REVISION
ARG VERSION

LABEL maintainer="Megabyte Labs <help@megabyte.space"
LABEL org.opencontainers.image.authors="Brian Zalewski <brian@megabyte.space>"
LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.description="Node.js files/configurations that support the creation of Dockerfiles"
LABEL org.opencontainers.image.documentation="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/tflint/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.revision=$REVISION
LABEL org.opencontainers.image.source="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/tflint.git"
LABEL org.opencontainers.image.url="https://megabyte.space"
LABEL org.opencontainers.image.vendor="Megabyte Labs"
LABEL org.opencontainers.image.version=$VERSION
LABEL space.megabyte.type="ci-pipeline"
